# CHANGES

## gem-grep 0.6.2 -- 2014-03-24

* Introduce method `grep_command` to make gem-grep more modular and therefore
  make gem-bgrep easier to reuse codes in gem-grep.

## gem-grep 0.6.1 -- 2014-03-24

* Fixed gem command description.
* Moved CapturedUI under Gem::Commands::GrepCommand.

## gem-grep 0.6.0 -- 2013-11-26

* Accept `$GEM_GREP` for the grep command. Recommend [ag][].
* Also pass -n (--line-number) to the grep command.

[ag]: https://github.com/ggreer/the_silver_searcher

## gem-grep 0.5.1 -- 2013-11-22

* Showed the grep command.

## gem-grep 0.5.0 -- 2013-11-22

* Birthday!
